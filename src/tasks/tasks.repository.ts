import { Task } from './task.entity';
import { EntityRepository, Repository } from 'typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskStatus } from './task-status.enum';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';

@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {

  // methods
  async getTasks(filterDto: GetTasksFilterDto): Promise<Task[]> {
    // get variables
    const { status, search } = filterDto;
    // set query
    const query = this.createQueryBuilder('task');
    // check status
    if (status) {
      query.andWhere('task.status = :status', { status });
    }
    // check search
    if (search) {
      query.andWhere(
        'LOWER(task.title) LIKE LOWER(:serach) OR LOWER(task.description) LIKE LIKE LOWER(:serach)',
        { search: `%${search}%`}
      )
    }
    // get tasks
    const tasks = await query.getMany();
    // return data
    return tasks;
  }

  async createTask(createTaskDto: CreateTaskDto): Promise<Task> {
    // get variables
    const { title, description } = createTaskDto;
    // set task
    const task = this.create({
      title,
      description,
      status: TaskStatus.OPEN
    })
    // save task
    await this.save(task);
    // return data
    return task;
  }
}
