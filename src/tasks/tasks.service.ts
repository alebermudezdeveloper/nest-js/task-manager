import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { TaskRepository } from './tasks.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { TaskStatus } from './task-status.enum';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TaskRepository)
    private taskRepository: TaskRepository,
  ) {}

  async getAllTasks(): Promise<Task[]> {
    return this.taskRepository.find();
  }

  getTasks(filterDto: GetTasksFilterDto): Promise<Task[]> {
    return this.taskRepository.getTasks(filterDto);
  }

  async getTaskById(id: number): Promise<Task> {
    // search task
    const found = await this.taskRepository.findOne(id);
    // check if task was found
    if (!found) {
      throw new NotFoundException(`Task with ID "${id}" not found`);
    }
    // return found task
    return found;
  }

  createTask(createTaskDto: CreateTaskDto): Promise<Task> {
    // create task
    return this.taskRepository.createTask(createTaskDto);
  }

  async updateTaskStatus(id: number, status: TaskStatus): Promise<Task> {
    // get task
    const task = await this.getTaskById(id);
    // update status
    task.status = status;
    // update task
    await task.save();
    // return data
    return task;
  }

  async deleteTask(id: number): Promise<void> {
    // delete task
    const result = await this.taskRepository.delete(id);
    // cehck if task was deleted
    if (result.affected === 0) {
      throw new NotFoundException(`Task with ID "${id}" not found`);
    }
  }

}
