import { EntityRepository, Repository } from "typeorm";
import { User } from "./user.entity";
import { AuthCredentialsDTO } from "./dto/auth-credentials.dto";

@EntityRepository(User)
export class UsersRepository extends Repository<User> {

  // methods
  async createUser(authCredentialsDTO: AuthCredentialsDTO): Promise<void> {
    // get data
    const { username, password } = authCredentialsDTO;
    // set user
    const user = this.create({ username, password });
    // save data
    await this.save(user);

  }

}