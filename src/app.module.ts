import { Module } from '@nestjs/common';
import { TasksModule } from './tasks/tasks.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'Th1nk.1n',
      database: 'task-management',
      entities: [__dirname + '/../**/*.entity.ts'],
      synchronize: true
    }),
    TasksModule,
    AuthModule,
  ],
})
export class AppModule {}
